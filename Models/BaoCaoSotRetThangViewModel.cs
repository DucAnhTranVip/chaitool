﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAITool.Models
{
    public class BaoCaoSotRetThangViewModel
    {
        public List<SelectListItem> ListTinh { get; set; }
        public List<SelectListItem> ListHuyen { get; set; }
        public List<SelectListItem> ListNam { get; set; }
        public List<SelectListItem> ListThang { get; set; }
        public List<SelectListItem> ListXa { get; set; }
        public List<SelectListItem> ListGiaiDoan { get; set; }

        public BaoCaoSotRetTab01 Tab01 { get; set; }
        public BaoCaoSotRetTab02 Tab02 { get; set; }
        public BaoCaoSotRetTab03 Tab03 { get; set; }
        public BaoCaoSotRetTab04 Tab04 { get; set; }

        public int? TongVatTu { get; set; }
        public string NhanXetVaDeNghi { get; set; }
        public int? NamBaoCao { get; set; }
        public int? ThangBaoCao { get; set; }
        public decimal? BaoCaoId { get; set; }
        public int? Action { get; set; }
        public string TenCoSo { get; set; }
        public int? CoSoID { get; set; }
        public int? LUU_STATUS { get; set; }
        public BaoCaoSotRetThangViewModel()
        {
            Tab01 = new BaoCaoSotRetTab01();
            Tab02 = new BaoCaoSotRetTab02();
            Tab03 = new BaoCaoSotRetTab03();
            Tab04 = new BaoCaoSotRetTab04();

        }
    }

    public class BaoCaoSotRetInput
    {
        public int? TabIndex { get; set; }
        public long? XaId { get; set; }
        public int? HuyenId { get; set; }
        public decimal? BaoCaoId { get; set; }
        public short? Year { get; set; }
        public short? Month { get; set; }
        public DateTime? NgayBaoCao { get; set; }

        public int? CoSoID { get; set; }
        public string TenCoSo { get; set; }
        public string TenHuyen { get; set; }

        public BaoCaoSotRetTab01 Tab01 { get; set; }
        public BaoCaoSotRetTab02 Tab02 { get; set; }
        public BaoCaoSotRetTab03 Tab03 { get; set; }
        public BaoCaoSotRetTab04 Tab04 { get; set; }

        public string NhanXetVaDeNghi { get; set; }
        public decimal? LUU_STATUS { get; set; }
        public BaoCaoSotRetInput()
        {
            Tab01 = new BaoCaoSotRetTab01();
            Tab02 = new BaoCaoSotRetTab02();
            Tab03 = new BaoCaoSotRetTab03();
            Tab04 = new BaoCaoSotRetTab04();
        }
    }

    public class BaoCaoSotRetTab01
    {
        public bool? LUUHANHSOTRET { get; set; }

        public int? SOTHON { get; set; }
        public int? SOHO { get; set; }
        public int? SODAN { get; set; }
        public int? SOOBENH { get; set; }

        public int? SOKST { get; set; }
        public int? SODIDAN { get; set; }
        public int? SONGUYCO { get; set; }

        public bool? KINHHV { get; set; }
        public bool? KINHHD { get; set; }

        public BaoCaoSotRetTab01()
        {
            //SOOBENH = 0;
            //SOKST = 0;
        }
    }

    public class BaoCaoSotRetTab02
    {
        public List<BaoCaoSotRetII> BaoCaoII { get; set; }
        public BaoCaoSotRetTab02()
        {
            BaoCaoII = new List<BaoCaoSotRetII>();
        }
    }
    public class BaoCaoSotRetII
    {
        public string STT { get; set; }
        [JsonIgnore]
        public string code { get; set; }
        public int? Id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        public int? SOLUONG { get; set; }
        public int? YTETHON { get; set; }
        public int? YTEXa { get; set; }
        public int? YTETUNHAN { get; set; }
        [JsonIgnore]
        public string Css { get; set; }
        [JsonIgnore]
        public bool? Disable { get; set; }
    }

    public class BaoCaoSotRetTab03
    {
        public List<BaoCaoSotRetIII> BaoCaoIII { get; set; }
        public List<BaoCaoSotRetIV> BaoCaoIV { get; set; }

        public BaoCaoSotRetTab03()
        {
            BaoCaoIII = new List<BaoCaoSotRetIII>();
            BaoCaoIV = new List<BaoCaoSotRetIV>();
        }
    }

    public class BaoCaoSotRetIII
    {
        public string code { get; set; }
        public int? PhuongPhapId { get; set; }
        public string PhuongPhapName { get; set; }
        public int? TongSoLuong { get; set; }
        public int? SotSoLuong { get; set; }

        public int? TongKySinh { get; set; }
        public int? NuKySinh { get; set; }
        public int? NamKySinh { get; set; }

        public decimal? TiLe { get; set; }

        public int? SoLuongPf { get; set; }
        public int? SoLuongPv { get; set; }
        public int? SoLuongPm { get; set; }
        public int? SoLuongPo { get; set; }
        public int? SoLuongPh { get; set; }
        public int? KySinhNoiDia { get; set; }
        [JsonIgnore]
        public string Disabled { get; set; }
    }

    public class BaoCaoSotRetIV
    {
        public string code { get; set; }
        public int? ThonId { get; set; }
        public string ThonName { get; set; }
        public int? SoLuongBenh { get; set; }
        public int? SoKST { get; set; }
        public int? SoLuongPf { get; set; }
        public int? SoLuongPv { get; set; }
        public int? SoLuongPm { get; set; }
        public int? SoLuongPo { get; set; }
        public int? SoLuongPh { get; set; }

        public int? SLAcTinh { get; set; }
        public int? SLAcTinh_KST { get; set; }
        public int? Lam { get; set; }
        public int? TestNhanh { get; set; }
        public int? LamVaTest { get; set; }
        public int? KSTNoiDia { get; set; }
        public int? OBenh { get; set; }
    }

    public class BaoCaoSotRetTab04
    {
        public List<BaoCaoSotRetV> BaoCaoV { get; set; }
        public List<BaoCaoSotRetVI> BaoCaoVI { get; set; }

        public BaoCaoSotRetTab04()
        {
            BaoCaoV = new List<BaoCaoSotRetV>();
            BaoCaoVI = new List<BaoCaoSotRetVI>();
        }
    }
    public class BaoCaoSotRetV
    {
        public string code { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? SoLuong { get; set; }
        public int? SoNguoi { get; set; }

        public int? DonViId { get; set; }
        public string DonViName { get; set; }
        public string TenXa { get; set; }
        public long? XaId { get; set; }
    }

    public class BaoCaoSotRetVI
    {
        public string code { get; set; }
        public decimal? VatTuId { get; set; }
        public string VatTuName { get; set; }
        public decimal? DonViId { get; set; }
        public string DonViName { get; set; }
        
        public decimal? TonThangTruoc { get; set; }
        public decimal? NhapTrongThang { get; set; }

        public decimal? DaDung { get; set; }
        public decimal? DieuChuyen { get; set; }
        public decimal? TonCuoiThang { get; set; }
        public string HanDung { get; set; }
        public string GhiChu { get; set; }

        //[JsonIgnore]
        public decimal? LoaiId { get; set; }
    }
}