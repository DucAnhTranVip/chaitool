﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAITool.Models
{
    public class ModelHuyen
    {
        public string Tinh { get; set; }
        public string Huyen { get; set; }
        public string ThangBC { get; set; }
        public string TinhTrangBC { get; set; }
        public string TongSoBNSRCKSTTTYT { get; set; }
        public string PFPHVCopfTTYT { get; set; }
        public string PVTTYT { get; set; }
        public string BNSotRetActinhTTYT { get; set; }
        public string SoBNCDSRTTYT { get; set; }
        public string XetNLTTYT { get; set; }
        public string XetNghiemTetCDNTTYT { get; set; }
        public string TongXetNghiemLamVaTCDNTTYT { get; set; }
        public string TongSoBNSRCKSTNTBV { get; set; }
        public string TongSoBNSRCKSTNgoaiTBV { get; set; }
        public string PFvaPHCpfBV { get; set; }
        public string PVBV { get; set; }
        public string BNSRATBV { get; set; }
        public string SoBNCDSRBV { get; set; }
        public string XetNghiemLBV { get; set; }
        public string XetNghiemTCDNBV { get; set; }
        public string TSXNLvaTCDNBV { get; set; }
        public string TSBNSRCKSTYTTN { get; set; }
        public string PFVPHCpfYTTN { get; set; }
        public string PVYTTN { get; set; }
        public string BNSRATYTTN { get; set; }
        public string BNCDSRYTTN { get; set; }
        public string GhiChu { get; set; }
    }
}