﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAITool.Models
{
    [Serializable]
    public class UserInfoBO
    {
        public decimal UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int? LoaiDVQL { get; set; }
        public short? RoleID { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public List<ChucNangBO> ListCN { get; set; }
        public List<ChucNangBO> ListCNFull { get; set; }
        public List<ThaoTacBO> ListThaoTac { get; set; }
        public List<ThaoTacBO> ListThaoTacPublic { get; set; }
        public List<ThaoTacBO> ListThaoTacAuthor { get; set; }
        public int? CoSoID { get; set; }
        public int? TinhID { get; set; }
        public int? HuyenID { get; set; }
        public long? XaID { get; set; }
        public decimal? VungMienID { get; set; }
        public string TenVungMien { get; set; }
        public List<short> TinhIDInVung { get; set; }
        // Phân loại tuyến, bệnh viện các cấp
        public int PhanLoaiCoSo { get; set; }
        public short? CapDo { get; set; }
        // Chinhnv12: bổ sung trạng thái để phân biệt user đang sử dụng hash SHA1 hay SHA256
        /// <summary>
        /// 1: dùng hash SHA256, 0: dùng sha1
        /// </summary>
        public short? TrangThai { get; set; }
        public short? Dm_VaiTro_id { get; set; }
        public string TenCoSo { get; set; }
        /// <summary>
        /// Phân biệt các nhóm quản lý: YTDP, BV, TT, QLY
        /// </summary>
        public short? NhomNghiepVu { get; set; }
        /// <summary>
        /// Cấp độ phân quyền của vai trò
        /// </summary>
        public short? DoUuTien { get; set; }
        // Moi Them
        public string TenDonVi { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PhanCap { get; set; }
        public string Address { get; set; }
        public decimal? LevelNguoiDung { get; set; }
        public decimal? VungMoRongID { get; set; }
        public int? CoSoChaId { get; set; }
        public UserInfoBO()
        {
            TinhIDInVung = new List<short>();
        }
        public bool IsTuyenVung()
        {
            string phanCap = this.PhanCap;
            bool isTuyenVung = false;
            if (!string.IsNullOrEmpty(phanCap))
            {
                isTuyenVung = phanCap.Contains("vung");
            }
            return isTuyenVung;
        }
    }
    public class UserInfoBC
    {
        public decimal UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int? LoaiDVQL { get; set; }
        public short? RoleID { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public List<ChucNangBO> ListCN { get; set; }
        public List<ChucNangBO> ListCNFull { get; set; }
        public List<ThaoTacBO> ListThaoTac { get; set; }
        public List<ThaoTacBO> ListThaoTacPublic { get; set; }
        public List<ThaoTacBO> ListThaoTacAuthor { get; set; }
        public int? CoSoID { get; set; }
        public int? TinhID { get; set; }
        public int? HuyenID { get; set; }
        public long? XaID { get; set; }
        public decimal? VungMienID { get; set; }
        public string TenVungMien { get; set; }
        public List<short> TinhIDInVung { get; set; }
        public int PhanLoaiCoSo { get; set; }
        public short? CapDo { get; set; }
        public short? TrangThai { get; set; }
        public short? Dm_VaiTro_id { get; set; }
        public string TenCoSo { get; set; }
        public short? NhomNghiepVu { get; set; }
        public short? DoUuTien { get; set; }
        public string TenDonVi { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PhanCap { get; set; }
        public string Address { get; set; }
        public decimal? LevelNguoiDung { get; set; }
        public decimal? VungMoRongID { get; set; }
        public int? CoSoChaId { get; set; }
        public UserInfoBC(UserInfoBO clone)
        {
            UserID = clone.UserID;
            Username = clone.Username;
            Password = clone.Password;
            LoaiDVQL = clone.LoaiDVQL;
            RoleID = clone.RoleID;
            PasswordSalt = clone.PasswordSalt;
            LastLoginDate = clone.LastLoginDate;
            ListCN = clone.ListCN;
            ListCNFull = clone.ListCNFull;
            ListThaoTac = clone.ListThaoTac;
            ListThaoTac = clone.ListThaoTac;
            ListThaoTacAuthor = clone.ListThaoTacAuthor;
            CoSoID = clone.CoSoID;
            TinhID = clone.TinhID;
            HuyenID = clone.HuyenID;
            XaID = clone.XaID;
            VungMienID = clone.VungMienID;
            TenVungMien = clone.TenVungMien;
            TinhIDInVung = clone.TinhIDInVung;
            PhanLoaiCoSo = clone.PhanLoaiCoSo;
            CapDo = clone.CapDo;
            TrangThai = clone.TrangThai;
            Dm_VaiTro_id = clone.Dm_VaiTro_id;
            TenCoSo = clone.TenCoSo;
            NhomNghiepVu = clone.NhomNghiepVu;
            DoUuTien = clone.DoUuTien;
            TenDonVi = clone.TenDonVi;
            Email = clone.Email;
            PhoneNumber = clone.PhoneNumber;
            PhanCap = clone.PhanCap;
            Address = clone.Address;
            LevelNguoiDung = clone.LevelNguoiDung;
            VungMoRongID = clone.VungMoRongID;
            CoSoChaId = clone.CoSoChaId;


        }
        public bool IsTuyenVung()
        {
            string phanCap = this.PhanCap;
            bool isTuyenVung = false;
            if (!string.IsNullOrEmpty(phanCap))
            {
                isTuyenVung = phanCap.Contains("vung");
            }
            return isTuyenVung;
        }
    }

    [Serializable]
    public class ChucNangBO
    {
        public short DM_CHUCNANG_ID { get; set; }
        public string TEN_CHUCNANG { get; set; }
        public short? TT_HIENTHI { get; set; }
        public string URL { get; set; }
        public int? CHUCNANG_CHA { get; set; }
        public short? MAC_DINH { get; set; }
        public short? IS_HIDDEN { get; set; }
        public string MA_CHUCNANG { get; set; }
        public List<ThaoTacBO> listThaoTac { get; set; }
    }

    [Serializable]
    public class ThaoTacBO
    {
        public short DM_CHUCNANG_ID { get; set; }
        public int DM_THAOTAC_ID { get; set; }
        public string TEN_THAOTAC { get; set; }
        public string MA_BUOC { get; set; }
        public string THAOTAC { get; set; }
        public string URL { get; set; }
        public int? TT_HIENTHI { get; set; }
        public string Tooltip { get; set; }
        public List<DateTime?> listThoiGian = new List<DateTime?>();
        public string FULL_NAMESPACE { get; set; }
        public string ACTION_CODE { get; set; }
        public string CONTROLLER_NAME { get; set; }
        public string ACTION_NAME { get; set; }
        public short? IS_ACTIVE { get; set; }
        public short? TYPE { get; set; }
        public short? IS_SHORTCUT { get; set; }
        public short PARAMETER_COUNT { get; set; }
        /// <summary>
        ///  Tính URL cho Action
        /// </summary>
        /// Đầu vào: Full_namespace + Action_name
        // 1. thêm "/" vào đầu. bỏ "Web."
        //2. bỏ "Controllers."
        //3. bỏ "Areas."
        //4. 
        //5. thay "." bằng "/"
        //6. thay "Controller" bằng "/"
        //7. thêm action_name vào cuối
        private void GetActionLink()
        {
            if (!string.IsNullOrEmpty(FULL_NAMESPACE))
            {
                URL = "/" + FULL_NAMESPACE.Replace("Web.", "").Replace("Controllers.", "").Replace("Areas.", "").Replace(".", "/").Replace("Controller", "/") + ACTION_NAME;
            }
        }
        private void GetActionCode()
        {
            ACTION_CODE = CONTROLLER_NAME + "_" + ACTION_NAME;
        }
        /// <summary>
        /// Tinh cac thong tin Thaotac khi load 1 thao tac moi tu assembly
        /// </summary>
        /// <param name="thaotac_param"> ThaoTacBO object</param>
        /// <returns></returns>
        public ThaoTacBO GetThaoTacBOInfo(ThaoTacBO thaotac_param)
        {
            GetActionCode();
            GetActionLink();
            return thaotac_param;
        }
    }
}