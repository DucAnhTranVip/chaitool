﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CHAITool.Models
{
    public class HuyenBaoCaoSotRetViewModel
    {
        public List<SelectListItem> ListTinh { get; set; }
        public List<SelectListItem> ListHuyen { get; set; }
        public List<SelectListItem> ListNam { get; set; }
        public List<SelectListItem> ListThang { get; set; }
        public List<SelectListItem> ListXa { get; set; }
        public List<SelectListItem> ListGiaiDoan { get; set; }

        public HuyenTab01 Tab01 { get; set; }
        public HuyenTab02 Tab02 { get; set; }
        public HuyenTab03 Tab03 { get; set; }
        public HuyenTab04 Tab04 { get; set; }
        public HuyenTab05 Tab05 { get; set; }
        public HuyenTab06 Tab06 { get; set; }

        public int? TongVatTu { get; set; }
        public int? TongSoXa { get; set; }
        public string NhanXetVaDeNghi { get; set; }
        public int? NamBaoCao { get; set; }
        public int? ThangBaoCao { get; set; }
        public decimal? BaoCaoId { get; set; }
        public bool IsFirstMonthReport { get; set; }
        public string TenCoSo { get; set; }
        public int? Action { get; set; }

        public decimal? LUU_STATUS { get; set; }
        public HuyenBaoCaoSotRetViewModel()
        {
            Tab01 = new HuyenTab01();
            Tab02 = new HuyenTab02();
            Tab03 = new HuyenTab03();
            Tab04 = new HuyenTab04();
            Tab05 = new HuyenTab05();
            Tab06 = new HuyenTab06();

        }
    }

    public class HuyenBaoCaoSotRetInput
    {
        public int? TabIndex { get; set; }
        public decimal? BaoCaoId { get; set; }
        public short? Year { get; set; }
        public short? Month { get; set; }
        public int? HuyenId { get; set; }
        public short? TinhId { get; set; }
        public decimal? LUU_STATUS { get; set; }
        public DateTime? NgayBaoCao { get; set; }

        public string TenTinh { get; set; }
        public string TenHuyen { get; set; }
        public string TenCoSo { get; set; }
        
        public HuyenTab01 Tab01 { get; set; }
        public HuyenTab02 Tab02 { get; set; }
        public HuyenTab03 Tab03 { get; set; }
        public HuyenTab04 Tab04 { get; set; }
        public HuyenTab05 Tab05 { get; set; }
        public HuyenTab06 Tab06 { get; set; }

        public HuyenBaoCaoSotRetInput()
        {
            Tab01 = new HuyenTab01();
            Tab02 = new HuyenTab02();
            Tab03 = new HuyenTab03();

            Tab04 = new HuyenTab04();
            Tab05 = new HuyenTab05();
            Tab06 = new HuyenTab06();
        }
    }

    public class HuyenTab01
    {
        public int? TONGSOHUYEN { get; set; }
        public int? TONGSOXA { get; set; }
        public int? SOTHON { get; set; }
        public int? SOHO { get; set; }
        public int? SODAN { get; set; }
        public int? XASOTNHE { get; set; }

        public int? DANSO { get; set; }
        public int? XASOTVUA { get; set; }
        public int? DANSOVUA { get; set; }

        public int? XASOTNANG { get; set; }
        public int? DANSONANG { get; set; }
        public int? SOOBENH { get; set; }
        public int? SOKST { get; set; }
        public int? SOCOSO { get; set; }
        public int? SOCOSO_BC { get; set; }
        public int? SOCOSO_HC { get; set; }
        public int? TONGDIEM_KHV { get; set; }
        public int? SODIEM_KHV { get; set; }
        public int? TONGDIEM_KHV_K { get; set; }
        public int? SODIEM_KHV_K { get; set; }

        public bool? KINHHV { get; set; }
        public bool? KINHHD { get; set; }

        public HuyenTab01()
        {
            SOOBENH = 0;
            SOKST = 0;
        }
    }

    public class HuyenTab02
    {
        public List<HuyenTable02> Table02 { get; set; }
        public HuyenTab02()
        {
            Table02 = new List<HuyenTable02>();
        }
    }
    public class HuyenTable02
    {
        [JsonIgnore]
        public string STT { get; set; }
        [JsonIgnore]
        public string code { get; set; }
        public int? Id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonIgnore]
        public int? SOLUONG { get; set; }
        public int? YTETHON { get; set; }
        public int? YTEXa { get; set; }
        public int? YTETUNHAN { get; set; }

        public int? TRUNGTAMYTE { get; set; }
        public int? NOITRU { get; set; }
        public int? NGOAITRU { get; set; }

        [JsonIgnore]
        public string Css { get; set; }
        [JsonIgnore]
        public bool? Disable { get; set; }
    }

    public class HuyenTab03
    {
        public List<HuyenTable03> Table03 { get; set; }
        public List<HuyenTable04> Table04 { get; set; }

        public HuyenTab03()
        {
            Table03 = new List<HuyenTable03>();
            Table04 = new List<HuyenTable04>();
        }
    }

    public class HuyenTable03
    {
        [JsonIgnore]
        public string code { get; set; }
        [JsonIgnore]
        public int? ParentId { get; set; }
        [JsonIgnore]
        public string NameParent { get; set; }

        public int? Id { get; set; }
        [JsonIgnore]
        public string PhuongPhapName { get; set; }
        public int? TongSoLuong { get; set; }
        public int? SotSoLuong { get; set; }

        [JsonIgnore]
        public int? TongKySinh { get; set; }
        public int? NuKySinh { get; set; }
        public int? NamKySinh { get; set; }

        [JsonIgnore]
        public decimal? TiLe { get; set; }

        public int? SoLuongPf { get; set; }
        public int? SoLuongPv { get; set; }
        public int? SoLuongPm { get; set; }
        public int? SoLuongPo { get; set; }
        public int? SoLuongPh { get; set; }
        public int? KySinhNoiDia { get; set; }
        [JsonIgnore]
        public string Disabled { get; set; }
    }

    public class HuyenTable04
    {
        [JsonIgnore]
        public string code { get; set; }
        public int? ThonId { get; set; }

        [JsonIgnore]
        public string ThonName { get; set; }

        public int? DanSo { get; set; }
        public int? SoLuongBenh { get; set; }
        
        public int? SoKST { get; set; }
        public int? SoLuongPf { get; set; }
        public int? SoLuongPv { get; set; }
        public int? SoLuongPm { get; set; }
        public int? SoLuongPo { get; set; }
        public int? SoLuongPh { get; set; }

        public int? SLAcTinh { get; set; }
        public int? SLAcTinh_KST { get; set; }

        [JsonIgnore]
        public int? TongXN { get; set; }
        public int? Lam { get; set; }
        public int? TestNhanh { get; set; }
        public int? LamVaTest { get; set; }
        public int? KSTNoiDia { get; set; }
        public int? OBenh { get; set; }
        public int? OBenh_KST { get; set; }
    }

    public class HuyenTab06
    {
        public List<HuyenTable09> Table09 { get; set; }
        public List<HuyenTable10> Table10 { get; set; }

        public string PhanTichTinhHinh { get; set; }
        public string DanhGiaHoatDong { get; set; }
        public string NhanXetVaDeNghi { get; set; }

        public HuyenTab06()
        {
            Table09 = new List<HuyenTable09>();
            Table10 = new List<HuyenTable10>();
        }
    }

    public class JsonNhanXet
    {
        public string PhanTichTinhHinh { get; set; }
        public string DanhGiaHoatDong { get; set; }
        public string NhanXetVaDeNghi { get; set; }
    }

    public class HuyenTable09
    {
        public string code { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }

        public int? SoLuong { get; set; }
        public int? SoLuong_Xa { get; set; }
        public int? SoLuong_Huyen { get; set; }

        public int? SoNguoi { get; set; }
        public int? SoNguoi_Xa { get; set; }
        public int? SoNguoi_Huyen { get; set; }
        
        public string DonViName { get; set; }

        [JsonIgnore]
        public string DonViNameDefault { get; set; }
        public int? DonViId { get; set; }
    }

    public class HuyenTable10 : BaoCaoSotRetVI
    {
      
    }

    public class HuyenTab04
    {
        public List<HuyenTable05> Table05 { get; set; }

        public int? BaoVeChung { get; set; }
        public int? DaThucHienBVC { get; set; }
        [JsonIgnore]
        public int? DatBVC { get; set; }

        public int? PhunTonLuu { get; set; }
        public int? DaThucHienPTL { get; set; }
        [JsonIgnore]
        public int? DatPTL { get; set; }

        public int? TamMan { get; set; }
        public int? DaThucHienTM { get; set; }
        [JsonIgnore]
        public int? DatTM { get; set; }

        public int? HoaChatPnId { get; set; }
        public int? HoaChatTamId { get; set; }

        public string HoaChatPnName { get; set; }
        public string HoaChatTamName { get; set; }

        public HuyenTab04()
        {
            Table05 = new List<HuyenTable05>();
        }
    }

    public class HuyenTable05
    {
        [JsonIgnore]
        public string code { get; set; }
        public int? id { get; set; }
        [JsonIgnore]
        public string name { get; set; }
        public int? DanSo { get; set; }

        public int? SoThonPn { get; set; }
        public int? SoHoPhun { get; set; }
        public int? DanSoPn { get; set; }
        public int? HoaChatPn { get; set; }

        public int? SoThonTam { get; set; }
        public int? SoHoTam { get; set; }
        public int? DanSoTam { get; set; }
        public int? HoaChatTam { get; set; }

        public int? SoThonPh { get; set; }
        public int? SoHoPh { get; set; }
        public int? DanSoPh { get; set; }
        public int? HoaChatPh { get; set; }

        public int? ManTam { get; set; }
        public int? DanSoBv { get; set; }

        [JsonIgnore]
        public int? TongDanBaoVe { get; set; }
    }

    public class HuyenTab05
    {
        public List<HuyenTable06> Table06 { get; set; }
        public List<HuyenTable07> Table07 { get; set; }
        public List<HuyenTable08> Table08 { get; set; }
        public HuyenTab05()
        {
            Table06 = new List<HuyenTable06>();
            Table07 = new List<HuyenTable07>();
            Table08 = new List<HuyenTable08>();
        }
    }

    public class HuyenTable06
    {
        public string code { get; set; }
        public string name { get; set; }
        public int? Lan { get; set; }
        public int? Diem { get; set; }
    }

    public class HuyenTable07
    {
        [JsonIgnore]
        public string code { get; set; }
        public int? id { get; set; }
        [JsonIgnore]
        public string name { get; set; }
        public int? OBenh { get; set; }
        public int? CanThiep { get; set; }
        public string GhiChu { get; set; }
    }

    public class HuyenTable08
    {
        public int? id { get; set; }
        public string name { get; set; }
        public int? CanBo { get; set; }
        public int? DaoTao { get; set; }
        public string NoiDung { get; set; }
    }
}