﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAITool.Models
{
    public class ModelXa
    {
        public string line { get; set; }
        public string Tinh { get; set; }
        public string Huyen{ get; set;}
        public string Xa{ get; set;}
        public string ThangBC{ get; set;}
        public string TinhTrangBC{ get; set;}
        public string TongBNSRCKST{ get; set;}
        public string pf{ get; set;}
        public string pv{ get; set;}
        public string pm{ get; set;}
        public string PhoiHop{ get; set;}
        public string BNSotRetActinh{ get; set;}
        public string KysinhtrungND{ get; set;}
        public string XetNL{ get; set;}
        public string XetNghiemTetCDN{ get; set;}
        public string TongXetNghiemLamVaTCDN{ get; set;}
        public string GhiChu{ get; set;}
    }
}