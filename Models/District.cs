﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAITool.Models
{
    public class District
    {
        public Province Province { get; set; }
        public string ProcinceId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string GsoId { get; set; }
    }
}