﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAITool.Models
{
    public class Ward
    {
        public District District { get; set; }
        public string DistrictId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string GsoId { get; set; }
    }
}