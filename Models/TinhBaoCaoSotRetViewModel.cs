﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CHAITool.Models
{
    public class TinhBaoCaoSotRetViewModel
    {
        public List<SelectListItem> ListTinh { get; set; }
        public List<SelectListItem> ListHuyen { get; set; }
        public List<SelectListItem> ListNam { get; set; }
        public List<SelectListItem> ListThang { get; set; }
        public List<SelectListItem> ListXa { get; set; }
        public List<SelectListItem> ListGiaiDoan { get; set; }
        public TinhHinhChung TinhHinhChung { get; set; }
        public List<BenhNhanSotRetVaDieuTri> BenhNhanSotRetVaDieuTri { get; set; }
        
        public XetNghiemVaPhanBoBenhNhan XetNghiemVaPhanBoBenhNhan { get; set; }
        public PhongChongVecto PhongChongVecto { get; set; }
        public GiamSatOBenhVaDaoTao GiamSatOBenhVaDaoTao { get; set; }
        public TuyenTruyenVatTuKhac TuyenTruyenVatTuKhac { get; set; }
        public int? TongVatTu { get; set; }
        public int? TongSoXa { get; set; }
        public string NhanXetVaDeNghi { get; set; }
        public int? NamBaoCao { get; set; }
        public int? ThangBaoCao { get; set; }
        public decimal? BaoCaoId { get; set; }
        public bool IsFirstMonthReport { get; set; }
        public string TenCoSo { get; set; }
        public int? Action { get; set; }
        public decimal? LUU_STATUS { get; set; }
        public TinhBaoCaoSotRetViewModel()
        {
            TinhHinhChung = new TinhHinhChung();
            BenhNhanSotRetVaDieuTri = new List<BenhNhanSotRetVaDieuTri>();
            XetNghiemVaPhanBoBenhNhan = new XetNghiemVaPhanBoBenhNhan();
            PhongChongVecto = new PhongChongVecto();
            GiamSatOBenhVaDaoTao = new GiamSatOBenhVaDaoTao();
            TuyenTruyenVatTuKhac = new TuyenTruyenVatTuKhac();
        }
    }


    public class XetNghiemVaPhanBoBenhNhan
    {
        public List<KetQuaXetNghiem> KetQuaXetNghiem { get; set; }
        public List<PhanBoBenhNhan> PhanBoBenhNhan { get; set; }

        public XetNghiemVaPhanBoBenhNhan()
        {
            KetQuaXetNghiem = new List<KetQuaXetNghiem>();
            PhanBoBenhNhan = new List<PhanBoBenhNhan>();
        }
    }


    public class TinhHinhChung
    {
        public int? TONGSOHUYEN { get; set; }
        public int? TONGSOXA { get; set; }
        public int? SOTHON { get; set; }
        public int? SOHO { get; set; }
        public int? SODAN { get; set; }
        public int? XASOTNHE { get; set; }

        public int? DANSO { get; set; }
        public int? XASOTVUA { get; set; }
        public int? DANSOVUA { get; set; }

        public int? XASOTNANG { get; set; }
        public int? DANSONANG { get; set; }
        public int? SOOBENH { get; set; }
        public int? SOKST { get; set; }
        public int? SOCOSO { get; set; }
        public int? SOCOSO_BC { get; set; }
        public int? SOCOSO_HC { get; set; }
        public int? TONGDIEM_KHV { get; set; }
        public int? SODIEM_KHV { get; set; }
        public int? TONGDIEM_KHV_K { get; set; }
        public int? SODIEM_KHV_K { get; set; }

        public bool? KINHHV { get; set; }
        public bool? KINHHD { get; set; }

        public TinhHinhChung()
        {
            SOOBENH = 0;
            SOKST = 0;
        }
    }

    



    public class BenhNhanSotRetVaDieuTri
    {
        [JsonIgnore]
        public string STT { get; set; }
        [JsonIgnore]
        public string code { get; set; }
        public int? Id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonIgnore]
        public int? SOLUONG { get; set; }
        public int? YTETHON { get; set; }
        public int? YTEXa { get; set; }
        public int? YTETUNHAN { get; set; }

        public int? TRUNGTAMYTE { get; set; }
        public int? NOITRU { get; set; }
        public int? NGOAITRU { get; set; }

        public int? YTDP { get; set; }
        public int? NOITRUTINH { get; set; }
        public int? NGOAITRUTINH { get; set; }

        [JsonIgnore]
        public string Css { get; set; }
        [JsonIgnore]
        public bool? Disable { get; set; }
    }


    public class KetQuaXetNghiem
    {
        [JsonIgnore]
        public string code { get; set; }
        [JsonIgnore]
        public int? ParentId { get; set; }
        [JsonIgnore]
        public string NameParent { get; set; }

        public int? Id { get; set; }
        [JsonIgnore]
        public string PhuongPhapName { get; set; }
        public int? TongSoLuong { get; set; }
        public int? SotSoLuong { get; set; }

        [JsonIgnore]
        public int? TongKySinh { get; set; }
        public int? NuKySinh { get; set; }
        public int? NamKySinh { get; set; }

        [JsonIgnore]
        public decimal? TiLe { get; set; }

        public int? SoLuongPf { get; set; }
        public int? SoLuongPv { get; set; }
        public int? SoLuongPm { get; set; }
        public int? SoLuongPo { get; set; }
        public int? SoLuongPh { get; set; }
        public int? KySinhNoiDia { get; set; }
        [JsonIgnore]
        public string Disabled { get; set; }
    }

    public class PhanBoBenhNhan
    {
        [JsonIgnore]
        public string code { get; set; }
        public int? ThonId { get; set; }

        [JsonIgnore]
        public string ThonName { get; set; }

        public int? DanSo { get; set; }
        public int? SoLuongBenh { get; set; }

        public int? SoKST { get; set; }
        public int? SoLuongPf { get; set; }
        public int? SoLuongPv { get; set; }
        public int? SoLuongPm { get; set; }
        public int? SoLuongPo { get; set; }
        public int? SoLuongPh { get; set; }

        public int? SLAcTinh { get; set; }
        public int? SLAcTinh_KST { get; set; }

        [JsonIgnore]
        public int? TongXN { get; set; }
        public int? Lam { get; set; }
        public int? TestNhanh { get; set; }
        public int? LamVaTest { get; set; }
        public int? KSTNoiDia { get; set; }
        public int? OBenh { get; set; }
        public int? OBenh_KST { get; set; }
    }


    public class PhongChongVecto
    {
        public List<BienPhapPhongChongMuoi> Table05 { get; set; }

        public int? BaoVeChung { get; set; }
        public int? DaThucHienBVC { get; set; }
        [JsonIgnore]
        public int? DatBVC { get; set; }

        public int? PhunTonLuu { get; set; }
        public int? DaThucHienPTL { get; set; }
        [JsonIgnore]
        public int? DatPTL { get; set; }

        public int? TamMan { get; set; }
        public int? DaThucHienTM { get; set; }
        [JsonIgnore]
        public int? DatTM { get; set; }

        public int? HoaChatPnId { get; set; }
        public int? HoaChatTamId { get; set; }

        public string HoaChatPnName { get; set; }
        public string HoaChatTamName { get; set; }

        public PhongChongVecto()
        {
            Table05 = new List<BienPhapPhongChongMuoi>();
        }
    }


    public class BienPhapPhongChongMuoi : HuyenTable05
    {
        
    }


    public class GiamSatOBenhVaDaoTao
    {
        public List<GiamSatHoatDongPCSotRet> GiamSatHoatDongPCSotRet { get; set; }
        public List<BaoCaoOBenhVaBienPhapGiaiQuyet> BaoCaoOBenhVaBienPhapGiaiQuyet { get; set; }
        public List<DaoTaoXayDungMangLuoi> DaoTaoXayDungMangLuoi { get; set; }
        public GiamSatOBenhVaDaoTao()
        {
            GiamSatHoatDongPCSotRet = new List<GiamSatHoatDongPCSotRet>();
            BaoCaoOBenhVaBienPhapGiaiQuyet = new List<BaoCaoOBenhVaBienPhapGiaiQuyet>();
            DaoTaoXayDungMangLuoi = new List<DaoTaoXayDungMangLuoi>();
        }
    }

    public class GiamSatHoatDongPCSotRet
    {
        public string code { get; set; }
        public string name { get; set; }
        public int? Lan { get; set; }
        public int? Diem { get; set; }
        public int? LanTinh { get; set; }
        public int? DiemTinh { get; set; }
        public int? LanTong { get; set; }
        public int DiemTong { get; set; }

    }

    public class BaoCaoOBenhVaBienPhapGiaiQuyet
    {
        [JsonIgnore]
        public string code { get; set; }
        public int? id { get; set; }
        [JsonIgnore]
        public string name { get; set; }
        public int? OBenh { get; set; }
        public int? CanThiep { get; set; }
        public string GhiChu { get; set; }
    }

    public class DaoTaoXayDungMangLuoi
    {
        public int? id { get; set; }
        public string name { get; set; }
        public int? CanBo { get; set; }
        public int? DaoTao { get; set; }
        public string NoiDung { get; set; }
        public bool Disabled { get; set; }
    }

    public class TuyenTruyenVatTuKhac
    {
        public List<TuyenTruyen> TuyenTruyen { get; set; }
        public List<VatTuHoaChat> VatTuHoaChat { get; set; }

        public string PhanTichTinhHinh { get; set; }
        public string DanhGiaHoatDong { get; set; }
        public string NhanXetVaDeNghi { get; set; }

        public TuyenTruyenVatTuKhac()
        {
            TuyenTruyen = new List<TuyenTruyen>();
            VatTuHoaChat = new List<VatTuHoaChat>();
        }
    }

    public class TuyenTruyen
    {
        public string code { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }

        public int? SoLuong { get; set; }
        public int? SoLuong_Xa { get; set; }
        public int? SoLuong_Huyen { get; set; }
        public int? SoLuong_Tinh { get; set; }

        public int? SoNguoi { get; set; }
        public int? SoNguoi_Xa { get; set; }
        public int? SoNguoi_Huyen { get; set; }
        public int? SoNguoi_Tinh { get; set; }
        public string DonViName { get; set; }
        [JsonIgnore]
        public string DonViNameDefault { get; set; }
        public int? DonViId { get; set; }
    }
    public class VatTuHoaChat : BaoCaoSotRetVI
    {
        public bool? IsHasHanDung { get; set; }
    }

    public class TinhBaoCaoSotRetInput
    {
        public int? TabIndex { get; set; }
        public decimal? BaoCaoId { get; set; }
        public short? Year { get; set; }
        public short? Month { get; set; }
        public int? HuyenId { get; set; }
        public short? TinhId { get; set; }
        public decimal? LUU_STATUS { get; set; }
        public DateTime? NgayBaoCao { get; set; }
        public decimal? CO_SO_ID { get; set; }
        public string TenTinh { get; set; }
        public string TenCoSo { get; set; }
        public string TenHuyen { get; set; }
        public string TenXa { get; set; }
        public TinhHinhChung TinhHinhChung { get; set; }
        public List<BenhNhanSotRetVaDieuTri> BenhNhanSotRetVaDieuTri { get; set; }
        public XetNghiemVaPhanBoBenhNhan XetNghiemVaPhanBoBenhNhan { get; set; }
        public PhongChongVecto PhongChongVecto { get; set; }
        public GiamSatOBenhVaDaoTao GiamSatOBenhVaDaoTao { get; set; }
        public TuyenTruyenVatTuKhac TuyenTruyenVatTuKhac { get; set; }

        public TinhBaoCaoSotRetInput()
        {
            TinhHinhChung = new TinhHinhChung();
            BenhNhanSotRetVaDieuTri = new List<BenhNhanSotRetVaDieuTri>();
            XetNghiemVaPhanBoBenhNhan = new XetNghiemVaPhanBoBenhNhan();

            PhongChongVecto = new PhongChongVecto();
            GiamSatOBenhVaDaoTao = new GiamSatOBenhVaDaoTao();
            TuyenTruyenVatTuKhac = new TuyenTruyenVatTuKhac();
        }
    }
}