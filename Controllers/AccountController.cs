﻿using CHAITool.Models;
using CHAITool.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CHAITool.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            if (Request.HttpMethod == "GET")
                return View();

            var jsonUser = System.IO.File.ReadAllText(Server.MapPath("~/DataFile/userinfojson.json"));

            var userInfo = JsonConvert.DeserializeObject<UserInfoBO>(jsonUser);

            var userName = Request.Form["Username"];
            var password = Request.Form["Password"];

            if (userName != userInfo.Username)
                return View();

            if (!MaHoaMatKhau.Encode_SHA256(password + userInfo.PasswordSalt).Equals(MaHoaMatKhau.Encode_SHA256("123456" + userInfo.PasswordSalt)))
                return View();

            HttpContext context = System.Web.HttpContext.Current;
            context.Session["UserInfo"] = userInfo;

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, "CCookie", DateTime.Now, DateTime.Now.AddMinutes(30), false, userInfo.Username.ToLower());
            string encryptedText = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie_new = new HttpCookie("CCOOKIE__", encryptedText);
            cookie_new.HttpOnly = true;
            Response.Cookies.Add(cookie_new);
            //Response.Cookies.Add(new HttpCookie("CCOOKIE__", encryptedText));

            FormsAuthentication.SetAuthCookie(userInfo.Username, false);
            System.Web.HttpContext.Current.Session.Remove("LoginFailCount");
            System.Web.HttpContext.Current.Session.Remove("Captcha");

            return RedirectToAction("Index", "Home");
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            System.Web.HttpContext.Current.Session.RemoveAll();
            Session.Abandon();
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, "CCookie", DateTime.Now, DateTime.Now.AddMinutes(30), false, "");
            string encryptedText = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie_new = new HttpCookie("CCOOKIE__", encryptedText);
            cookie_new.HttpOnly = true;
            Response.Cookies.Add(cookie_new);
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return RedirectToAction("Login");
        }
    }
}