﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.OleDb;
using CHAITool.Models;
using MyMvcProject.Filters;
using ClosedXML.Excel;
using CHAITool.Utils;
using Microsoft.Office.Interop.Excel;

namespace CHAITool.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [Throttle(TimeUnit = TimeUnit.Minute, Count = 5)]
        [Throttle(TimeUnit = TimeUnit.Hour, Count = 20)]
        [Throttle(TimeUnit = TimeUnit.Day, Count = 100)]
        [ValidateInput(true)]
        public ActionResult UploadFile()
        {
            try
            {
                HttpPostedFileBase file = Request.Files[0];
                if (file.ContentLength > 0 && !System.IO.File.Exists("D:/VisualProject/CHAITool/SaveFileTinh/" + file.FileName))
                {
                    var fileName = Path.GetFileName(file.FileName);
                    file.SaveAs(Path.Combine(Server.MapPath("~/SaveFileTinh/"), fileName));
                }
                return View("Index");
            }
            catch (Exception e)
            {
                return View("Error", e);
            }
        }
        [Throttle(TimeUnit = TimeUnit.Minute, Count = 5)]
        [Throttle(TimeUnit = TimeUnit.Hour, Count = 20)]
        [Throttle(TimeUnit = TimeUnit.Day, Count = 100)]
        [ValidateInput(true)]
        [HttpPost]
        public ActionResult HienThiXaValidate()
        {
            try
            {
                //string startupPath = System.IO.Directory.GetCurrentDirectory();
                //string startupPath2 = Environment.CurrentDirectory;
                //var spreadsheetLocation = Path.Combine(Directory.GetCurrentDirectory(), "Hà Nội Historical Data Collection.xlsx");
                HttpPostedFileBase file = Request.Files[0];
                if (file.ContentLength > 0 && !System.IO.File.Exists("D:/VisualProject/CHAITool/SaveFileTinh/" + file.FileName))
                {
                    var fileName = Path.GetFileName(file.FileName);
                }
                //Create COM Objects. Create a COM object for everything that is referenced

                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open("D:/VisualProject/CHAITool/SaveFileTinh/" + file.FileName, 2, false);
                Excel._Worksheet xlWorksheet6 = xlWorkbook.Sheets[6];
                Excel.Range xlRange = xlWorksheet6.UsedRange;

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                //iterate over the rows and columns and stored in Province as it appears in the file
                //excel is not zero based!!

                //get idXa from sheet 6 Xa
                string[] IdXa = new string[rowCount - 1];
                //lmx lưu data xã trong excel
                List<ModelXa> lmx = new List<ModelXa>();
                for (int i = 0; i < rowCount - 1; i++)
                {
                    IdXa[i] = (xlWorksheet6.Cells[i + 2, 3] as Excel.Range).Value2.ToString();
                    //Luu thông tin xa vào list model xa
                    ModelXa mx = new ModelXa()
                    {
                        line = (i + 2).ToString(),
                        Tinh = (xlWorksheet6.Cells[i + 2, 1] as Excel.Range).Value2.ToString(),
                        Huyen = (xlWorksheet6.Cells[i + 2, 2] as Excel.Range).Value2.ToString(),
                        Xa = (xlWorksheet6.Cells[i + 2, 3] as Excel.Range).Value2.ToString(),
                        ThangBC = (xlWorksheet6.Cells[i + 2, 4] as Excel.Range).Value2.ToString(),
                        TinhTrangBC = (xlWorksheet6.Cells[i + 2, 5] as Excel.Range).Value2.ToString(),
                        TongBNSRCKST = (xlWorksheet6.Cells[i + 2, 6] as Excel.Range).Value2.ToString(),
                        pf = (xlWorksheet6.Cells[i + 2, 7] as Excel.Range).Value2.ToString(),
                        pv = (xlWorksheet6.Cells[i + 2, 8] as Excel.Range).Value2.ToString(),
                        pm = (xlWorksheet6.Cells[i + 2, 9] as Excel.Range).Value2.ToString(),
                        PhoiHop = (xlWorksheet6.Cells[i + 2, 10] as Excel.Range).Value2.ToString(),
                        BNSotRetActinh = (xlWorksheet6.Cells[i + 2, 11] as Excel.Range).Value2.ToString(),
                        KysinhtrungND = (xlWorksheet6.Cells[i + 2, 12] as Excel.Range).Value2.ToString(),
                        XetNL = (xlWorksheet6.Cells[i + 2, 13] as Excel.Range).Value2.ToString(),
                        XetNghiemTetCDN = (xlWorksheet6.Cells[i + 2, 14] as Excel.Range).Value2.ToString(),
                        TongXetNghiemLamVaTCDN = (xlWorksheet6.Cells[i + 2, 15] as Excel.Range).Value2.ToString()
                    };
                    lmx.Add(mx);

                }


                //ValidateidHuyen
                Excel._Worksheet xlWorksheet7 = xlWorkbook.Sheets[7];
                Excel.Range xlRangeH = xlWorksheet7.UsedRange;
                int rowCountH = xlRangeH.Rows.Count;
                //lấy IdHuyen trong excel
                string[] IdHuyen = new string[rowCountH - 1];

                //Add lưu data Huyen trong excel vào lmh
                List<ModelHuyen> lmh = new List<ModelHuyen>();
                for (int i = 0; i < rowCountH - 1; i++)
                {
                    IdHuyen[i] = (xlWorksheet7.Cells[i + 2, 2] as Excel.Range).Value2.ToString();

                    //Luu thông tin xa vào list model xa
                    ModelHuyen mh = new ModelHuyen()
                    {
                        Tinh = (xlWorksheet7.Cells[i + 2, 1] as Excel.Range).Value2.ToString(),
                        Huyen = (xlWorksheet7.Cells[i + 2, 2] as Excel.Range).Value2.ToString(),
                        ThangBC = (xlWorksheet7.Cells[i + 2, 3] as Excel.Range).Value2.ToString(),
                        TinhTrangBC = (xlWorksheet7.Cells[i + 2, 4] as Excel.Range).Value2.ToString(),
                        TongSoBNSRCKSTTTYT = (xlWorksheet7.Cells[i + 2, 5] as Excel.Range).Value2.ToString(),
                        PFPHVCopfTTYT = (xlWorksheet7.Cells[i + 2, 6] as Excel.Range).Value2.ToString(),
                        PVTTYT = (xlWorksheet7.Cells[i + 2, 7] as Excel.Range).Value2.ToString(),
                        BNSotRetActinhTTYT = (xlWorksheet7.Cells[i + 2, 8] as Excel.Range).Value2.ToString(),
                        SoBNCDSRTTYT = (xlWorksheet7.Cells[i + 2, 9] as Excel.Range).Value2.ToString(),
                        XetNLTTYT = (xlWorksheet7.Cells[i + 2, 10] as Excel.Range).Value2.ToString(),
                        TongXetNghiemLamVaTCDNTTYT = (xlWorksheet7.Cells[i + 2, 11] as Excel.Range).Value2.ToString(),
                        TongSoBNSRCKSTNTBV = (xlWorksheet7.Cells[i + 2, 12] as Excel.Range).Value2.ToString(),
                        TongSoBNSRCKSTNgoaiTBV = (xlWorksheet7.Cells[i + 2, 13] as Excel.Range).Value2.ToString(),
                        PFvaPHCpfBV = (xlWorksheet7.Cells[i + 2, 14] as Excel.Range).Value2.ToString(),
                        PVBV = (xlWorksheet7.Cells[i + 2, 15] as Excel.Range).Value2.ToString(),
                        BNSRATBV = (xlWorksheet7.Cells[i + 2, 16] as Excel.Range).Value2.ToString(),
                        SoBNCDSRBV = (xlWorksheet7.Cells[i + 2, 17] as Excel.Range).Value2.ToString(),
                        XetNghiemLBV = (xlWorksheet7.Cells[i + 2, 18] as Excel.Range).Value2.ToString(),
                        XetNghiemTCDNBV = (xlWorksheet7.Cells[i + 2, 19] as Excel.Range).Value2.ToString(),
                        TSXNLvaTCDNBV = (xlWorksheet7.Cells[i + 2, 20] as Excel.Range).Value2.ToString(),
                        TSBNSRCKSTYTTN = (xlWorksheet7.Cells[i + 2, 21] as Excel.Range).Value2.ToString(),
                        PFVPHCpfYTTN = (xlWorksheet7.Cells[i + 2, 22] as Excel.Range).Value2.ToString(),
                        BNSRATYTTN = (xlWorksheet7.Cells[i + 2, 24] as Excel.Range).Value2.ToString(),
                        BNCDSRYTTN = (xlWorksheet7.Cells[i + 2, 25] as Excel.Range).Value2.ToString()
                        //mh.GhiChu = (xlWorksheet7.Cells[i + 2, 15] as Excel.Range).Value2.ToString();
                    };
                    lmh.Add(mh);
                }


                //Tìm trong listDistrict
                var resultHuyen = IdHuyen.Where(ExidH => !Utils.Category.LstDistrict.Exists(dst => dst.Id == ExidH)).ToList();

                //for (int i = 0; i < rowCountH - 1; i++)
                //{
                //    ModelHuyen mh = new ModelHuyen();
                //    mh.GhiChu = resultHuyen[i];
                //    lmh.Add(mh);
                //}
                //Add idHuyen lỗi vào lmh.GhiChu
                foreach (var h in lmh)
                {
                    h.GhiChu = resultHuyen.Find(idloi => idloi.Equals(h.Huyen)).ToString();
                }

                lmh.Where(x => resultHuyen.Exists(id => id.Equals(x.Huyen))).ToList().ForEach(x => x.GhiChu = "Lỗi");
                //Tìm trong listWard nếu mapped lưu vào resultXa, id của xã đó
                var resultXa = IdXa.Where(ExIdX => !Utils.Category.LstWard.Exists(xa => xa.Id == ExIdX)).ToList();
                //Add idXa vào ghi chú
                foreach (var x in lmx)
                {
                    x.GhiChu = resultXa.Find(idloi => idloi.Equals(x.Xa));
                }
                lmx.Where(x => resultXa.Exists(id => id.Equals(x.Xa))).ToList().ForEach(x => x.GhiChu = "Lỗi line " + x.line);

                //Add dataXa có Id trong listward vào object baocaotab3.BaoCaoIV
                BaoCaoSotRetTab03 bctab3 = new BaoCaoSotRetTab03();

                List<BaoCaoSotRetIV> bc4 = new List<BaoCaoSotRetIV>();
                AddToBaoCaoSotRetIV(lmx, ref bc4);
                bctab3.BaoCaoIV = bc4;
                List<BaoCaoSotRetIII> bc3 = new List<BaoCaoSotRetIII>();
                AddToBaoCaoSotRetIII(lmx, ref bc3);
                bctab3.BaoCaoIII = bc3;

                //Write to excel
                WriteToExcel(rowCount, ref lmx, xlWorkbook);

                //clean up
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //release com objects to fully kill excel process from running in the background
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet6);

                //close and release
                xlWorkbook.Close(true);
                Marshal.FinalReleaseComObject(xlWorkbook);

                //quit and release
                xlApp.Quit();
                Marshal.FinalReleaseComObject(xlApp);


                return PartialView("UploadFileXa", lmx);
            }
            catch (Exception e)
            {
                return View("Error", e);
            }

        }
        public void AddToBaoCaoSotRetIII(List<ModelXa> lmx, ref List<BaoCaoSotRetIII> bc3)
        {
            foreach (ModelXa mx in lmx)
            {
                if (mx.GhiChu != null)
                {
                    BaoCaoSotRetIII bc = new BaoCaoSotRetIII()
                    {

                        SoLuongPf = Int32.Parse(mx.pf),
                        SoLuongPv = Int32.Parse(mx.pv),
                        SoLuongPm = Int32.Parse(mx.pm),
                        SoLuongPh = Int32.Parse(mx.PhoiHop)
                    };
                    bc3.Add(bc);
                }
            }
        }
        public void AddToBaoCaoSotRetIV(List<ModelXa> lmx, ref List<BaoCaoSotRetIV> bc4)
        {
            foreach (ModelXa mx in lmx)
            {
                if (mx.GhiChu != null)
                {
                    BaoCaoSotRetIV bc = new BaoCaoSotRetIV()
                    {
                        ThonId = Int32.Parse(mx.Xa),
                        SoKST = Int32.Parse(mx.TongBNSRCKST),
                        SoLuongPf = Int32.Parse(mx.pf),
                        SoLuongPv = Int32.Parse(mx.pv),
                        SoLuongPm = Int32.Parse(mx.pm),
                        SoLuongPh = Int32.Parse(mx.PhoiHop),
                        SLAcTinh = Int32.Parse(mx.BNSotRetActinh),
                        KSTNoiDia = Int32.Parse(mx.KysinhtrungND),
                        Lam = Int32.Parse(mx.XetNL),
                        TestNhanh = Int32.Parse(mx.XetNghiemTetCDN),
                        LamVaTest = Int32.Parse(mx.TongXetNghiemLamVaTCDN)
                    };
                    bc4.Add(bc);
                }
            }
        }
        public void AddToPhanBoBenhNhan(List<ModelXa> lmx, ref List<PhanBoBenhNhan> pbbn)
        {
            foreach (ModelXa mx in lmx)
            {
                if (mx.GhiChu != null)
                {
                    PhanBoBenhNhan pb = new PhanBoBenhNhan()
                    {
                        code = mx.Xa,
                        KSTNoiDia = Int32.Parse(mx.KysinhtrungND),
                        Lam = Int32.Parse(mx.XetNL),
                        LamVaTest = Int32.Parse(mx.TongXetNghiemLamVaTCDN),
                        SLAcTinh = Int32.Parse(mx.BNSotRetActinh),
                        SLAcTinh_KST = Int32.Parse(mx.TongBNSRCKST),
                        SoLuongPf = Int32.Parse(mx.pf),
                        SoLuongPm = Int32.Parse(mx.pm),
                        SoLuongPv = Int32.Parse(mx.pv),
                        TestNhanh = Int32.Parse(mx.XetNghiemTetCDN)
                    };
                    pbbn.Add(pb);
                }
            }
        }
        public void AddToKetQuaXetNghiem(List<ModelXa> lmx, ref List<KetQuaXetNghiem> kqxn)
        {
            foreach (ModelXa mx in lmx)
            {
                if (mx.GhiChu != null)
                {
                    KetQuaXetNghiem pb = new KetQuaXetNghiem()
                    {
                        Id = Int32.Parse(mx.Xa),
                        KySinhNoiDia = Int32.Parse(mx.KysinhtrungND),
                        TongKySinh = Int32.Parse(mx.TongBNSRCKST),
                        SoLuongPf = Int32.Parse(mx.pf),
                        SoLuongPm = Int32.Parse(mx.pm),
                        SoLuongPv = Int32.Parse(mx.pv)
                    };
                    kqxn.Add(pb);
                }
            }
        }
        public void WriteToExcel(int rowCount, ref List<ModelXa> lmx, Excel.Workbook xlWorkBook)
        {
            Excel._Worksheet xlWorkSheet =  xlWorkBook.Sheets[9];

            xlWorkSheet.Cells[1, 1] = "Tinh";
            xlWorkSheet.Cells[1, 2] = "Huyen";
            xlWorkSheet.Cells[1, 3] = "Xa";
            xlWorkSheet.Cells[1, 4] = "TinhTrangBaoCao";
            xlWorkSheet.Cells[1, 5] = "ThangBaoCao";
            xlWorkSheet.Cells[1, 6] = "TongBenNhanSotRetKST";
            xlWorkSheet.Cells[1, 7] = "pf";
            xlWorkSheet.Cells[1, 8] = "pv";
            xlWorkSheet.Cells[1, 9] = "pm";
            xlWorkSheet.Cells[1, 10] = "PhoiHop";
            xlWorkSheet.Cells[1, 11] = "BenhNhanSotRetAcTinh";
            xlWorkSheet.Cells[1, 12] = "KySinhTrungNoiDia";
            xlWorkSheet.Cells[1, 13] = "XetNghiemLam";
            xlWorkSheet.Cells[1, 14] = "XetNghiemTetChuanDoanNhanh";
            xlWorkSheet.Cells[1, 15] = "TongXetNghiemLamvaTetChuanDoanNhanh";
            xlWorkSheet.Cells[1, 16] = "GhiChu";

            for (int i = 0; i < rowCount - 1; i++)
            {
                xlWorkSheet.Cells[i + 2, 1] = lmx[i].Tinh;
                xlWorkSheet.Cells[i + 2, 2] = lmx[i].Huyen;
                xlWorkSheet.Cells[i + 2, 3] = lmx[i].Xa;
                xlWorkSheet.Cells[i + 2, 4] = lmx[i].ThangBC;
                xlWorkSheet.Cells[i + 2, 5] = lmx[i].TinhTrangBC;
                xlWorkSheet.Cells[i + 2, 6] = lmx[i].TongBNSRCKST;
                xlWorkSheet.Cells[i + 2, 7] = lmx[i].pf;
                xlWorkSheet.Cells[i + 2, 8] = lmx[i].pv;
                xlWorkSheet.Cells[i + 2, 9] = lmx[i].pm;
                xlWorkSheet.Cells[i + 2, 10] = lmx[i].PhoiHop;
                xlWorkSheet.Cells[i + 2, 11] = lmx[i].BNSotRetActinh;
                xlWorkSheet.Cells[i + 2, 12] = lmx[i].KysinhtrungND;
                xlWorkSheet.Cells[i + 2, 13] = lmx[i].XetNL;
                xlWorkSheet.Cells[i + 2, 14] = lmx[i].XetNghiemTetCDN;
                xlWorkSheet.Cells[i + 2, 15] = lmx[i].TongXetNghiemLamVaTCDN;
                xlWorkSheet.Cells[i + 2, 16] = lmx[i].GhiChu;

                //mh.GhiChu = (xlWorksheet7.Cells[i + 2, 15] as Excel.Range).Value2.ToString();
            }

        }
        public void AddToHuyenTable04(List<ModelHuyen> lmh, ref List<HuyenTable04> hbc)
        {
            foreach (ModelHuyen mh in lmh)
            {
                if (mh.GhiChu != null)
                {
                    HuyenTable04 bc = new HuyenTable04()
                    {
                        code = mh.Huyen,
                        KSTNoiDia = Int32.Parse(mh.TongSoBNSRCKSTNTBV) + Int32.Parse(mh.TongSoBNSRCKSTTTYT),
                        Lam = Int32.Parse(mh.XetNghiemLBV),
                        LamVaTest = Int32.Parse(mh.TongXetNghiemLamVaTCDNTTYT),
                        SLAcTinh = Int32.Parse(mh.BNSotRetActinhTTYT),
                        SoLuongPf = Int32.Parse(mh.PFPHVCopfTTYT) + Int32.Parse(mh.PFvaPHCpfBV) + Int32.Parse(mh.PFVPHCpfYTTN),
                        SoLuongPv = Int32.Parse(mh.PVBV) + Int32.Parse(mh.PVTTYT)
                    };
                    hbc.Add(bc);
                }
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}