﻿using CHAITool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Web.Hosting;

namespace CHAITool.Utils
{
    public static class Category
    {
        private static List<Province> Provinces = null;
        private static List<District> Districts = null;
        private static List<Ward> Wards = null;

        public static List<Province> LstProvince
        {
            get
            {
                if (Provinces != null)
                    return Provinces;
                Provinces = new List<Province>();
                var lines = File.ReadAllLines(HostingEnvironment.MapPath("~/DataFile/listprovince.txt"));
                if (lines != null && lines.Length > 0)
                {
                    foreach (var item in lines)
                    {
                        var sp = item.Split('|');
                        if (sp.Length == 3)
                        {
                            var obj = new Province()
                            {
                                Id = sp[0],
                                Name = sp[1],
                                GsoId = sp[2]
                            };
                            Provinces.Add(obj);
                        }
                    }
                }
                return Provinces;
            }
        }
        public static List<District> LstDistrict
        {
            get
            {
                if (Districts != null)
                    return Districts;
                Districts = new List<District>();
                var lines = File.ReadAllLines(HostingEnvironment.MapPath("~/DataFile/listdistrict.txt"));
                if (lines != null && lines.Length > 0)
                {
                    foreach (var item in lines)
                    {
                        var sp = item.Split('|');
                        if (sp.Length == 4)
                        {
                            var obj = new District()
                            {
                                ProcinceId = sp[0],
                                Id = sp[1],
                                Name = sp[2],
                                GsoId = sp[3]
                            };
                            Districts.Add(obj);
                        }
                    }
                }
                return Districts;
            }
        }
        public static List<Ward> LstWard
        {
            get
            {
                if (Wards != null)
                    return Wards;
                Wards = new List<Ward>();
                var lines = File.ReadAllLines(HostingEnvironment.MapPath("~/DataFile/listward.txt"));
                if (lines != null && lines.Length > 0)
                {
                    foreach (var item in lines)
                    {
                        var sp = item.Split('|');
                        if (sp.Length == 4)
                        {
                            var obj = new Ward()
                            {
                                DistrictId = sp[0],
                                Id = sp[1],
                                Name = sp[2],
                                GsoId = sp[3]
                            };
                            Wards.Add(obj);
                        }
                    }
                }
                return Wards;
            }
        }

    }
}